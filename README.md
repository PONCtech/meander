# Meander

## Introduction

Meander is a light-weight flow-based programming framework for [go](https://go.dev/). The philosophy
is that every operation consists of input, transformation, output, and error handling. Meander
facilitates this by having carriers for all the I/O actions, while having transformation plugins
for the transformation between the input and output.

## Installation

Installation is done like any go library:
```sh
$ go get gitlab.com/PONCtech/meander
```

## Work in progress

Meander is in its early stages, more documentation is to follow, but some examples of its usages
can be found in the `examples/` directory.
