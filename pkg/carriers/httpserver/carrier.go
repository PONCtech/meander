/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package httpserver

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"syscall"
	"time"

	"github.com/google/uuid"
	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
	"golang.org/x/crypto/acme/autocert"
)

type MessageStatus uint8

const (
	MessagePending MessageStatus = iota
	MessageFailed
	MessageSuccess
)

const (
	defaultHTTP  = 80
	defaultHTTPS = 443
)

// sleepTime in ms to wait between polling message status.
const sleepTime = 100

type HTTPRequestHandler func(
	req *http.Request,
	logger journal.Logger,
	c *Carrier,
	inChan chan *flow.MessageContext,
	w http.ResponseWriter,
)

// Carrier signifies the http server input carrier for the meander flow framework.
type Carrier struct {
	Config        *Config
	Logger        journal.Logger
	Context       context.Context
	MessageLookup map[uuid.UUID]MessageStatus
	PostHandler   HTTPRequestHandler

	sync.Mutex
}

func New(ctx context.Context, logger journal.Logger, config *Config, postHandler HTTPRequestHandler) (*Carrier, error) {
	ml := make(map[uuid.UUID]MessageStatus)
	if !checkListenAddress(config.ListenAddress) {
		return nil, fmt.Errorf("not a valid listen address: %s", config.ListenAddress)
	}

	if config.EnableTLS {
		if config.Port == 0 {
			config.Port = defaultHTTPS
		}

		if config.Port != defaultHTTPS {
			return nil, fmt.Errorf("port can only be 443 when TLS enabled")
		}

		if err := checkCertConf(config); err != nil {
			return nil, fmt.Errorf("SSL configuration invalid: %w", err)
		}
	} else if config.Port == 0 {
		config.Port = defaultHTTP
	}

	if err := checkBasicAuth(config); err != nil {
		return nil, err
	}

	addLoggerFields(logger, config)

	postHandlerFunction := handlePost
	if postHandler != nil {
		postHandlerFunction = postHandler
	}

	return &Carrier{
		Config:        config,
		Logger:        logger,
		Context:       ctx,
		MessageLookup: ml,
		PostHandler:   postHandlerFunction,
	}, nil
}

func addLoggerFields(logger journal.Logger, config *Config) {
	logger.With(
		journal.Int("port", config.Port),
		journal.Bool("https", config.EnableTLS),
		journal.Bool("letsencrypt", config.EnableLetsEncrypt),
		journal.String("listen-address", config.ListenAddress),
		journal.Strings("hostnames", config.Hostnames),
		journal.String("certificate", config.CertFiles.Certificate),
		journal.String("key", config.CertFiles.Key),
		journal.Bool("basic-auth",
			config.BasicAuth != nil &&
				config.BasicAuth.Username != "" &&
				config.BasicAuth.Password != "" &&
				config.BasicAuth.Realm != ""),
		journal.String("cert-directory", config.CertDirectory),
	)
}

func checkBasicAuth(config *Config) error {
	if config.BasicAuth != nil {
		if config.BasicAuth.Username == "" || config.BasicAuth.Password == "" || config.BasicAuth.Realm == "" {
			return fmt.Errorf("need all of username/password/realm defined")
		}
	}
	return nil
}

// createHandler creates a handler function that is aware of the channel.
func (c *Carrier) createHandler(inChan chan *flow.MessageContext) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		logger := c.Logger.WithCopy(
			journal.String("path", req.URL.Path),
			journal.String("method", req.Method),
			journal.String("remote", req.RemoteAddr),
		)
		logger.Info("handling http request")

		var ok bool
		var username string
		if username, ok = authSuccessful(logger, c.Config.BasicAuth, w, req); !ok {
			return
		}
		logger.With(journal.String("username", username))

		if req.URL.Path != "/" {
			logger.Error("Invalid path.")
			http.Error(w, "Not found.", http.StatusNotFound)
			return
		}

		switch req.Method {
		case "POST":
			logger.Debug("Handling post")
			c.PostHandler(req, logger, c, inChan, w)
		default:
			logger.Error("Invalid method")
			http.Error(w, "Method not allowed.", http.StatusMethodNotAllowed)
		}
	}
}

func handlePost(
	req *http.Request,
	logger journal.Logger,
	c *Carrier,
	inChan chan *flow.MessageContext,
	w http.ResponseWriter,
) {
	body, err := io.ReadAll(req.Body)
	if err != nil {
		logger.Error("failed to read body", journal.Error(err))
	}
	messageWrapper := flow.NewWrapper(
		body,
		"httpBody",
		req.RemoteAddr,
	)
	c.Lock()
	c.MessageLookup[messageWrapper.GetID()] = MessagePending
	c.Unlock()
	inChan <- flow.NewMessageContext(
		messageWrapper,
		logger,
		messageWrapper.Type,
		nil,
	)

ResultLoop:
	for {
		c.Lock()
		status, ok := c.MessageLookup[messageWrapper.GetID()]
		c.Unlock()
		if !ok {
			break
		}
		switch status {
		case MessagePending:
			time.Sleep(sleepTime * time.Millisecond)
		case MessageSuccess:
			logger.Info("messaged successfully processed")
			w.WriteHeader(http.StatusAccepted)
			fmt.Fprintln(w, "Accepted")
			break ResultLoop
		case MessageFailed:
			logger.Warn("message not successfully processed")
			http.Error(w, "Failed", http.StatusInternalServerError)
			break ResultLoop
		}
	}
	c.Lock()
	delete(c.MessageLookup, messageWrapper.GetID())
	c.Unlock()
}

func authSuccessful(
	logger journal.Logger,
	basicAuth *BasicAuth,
	w http.ResponseWriter,
	req *http.Request,
) (string, bool) {
	if basicAuth == nil {
		logger.Debug("Basic auth disabled, passing")
		return "", true
	}

	username, password, ok := req.BasicAuth()
	logger.With(journal.String("username", username))
	if ok && username == basicAuth.Username && password == basicAuth.Password {
		logger.Info("Auth successful")
		return username, true
	}

	logger.Info("Auth failed")
	w.Header().Add("WWW-Authenticate", fmt.Sprintf(`Basic realm="%s"`, basicAuth.Realm))
	w.WriteHeader(http.StatusUnauthorized)
	return username, false
}

func (c *Carrier) SetupInput(inChan chan *flow.MessageContext) error {
	go func() {
		c.Logger.Info(
			"Starting web server",
			journal.String("address", c.Config.ListenAddress),
			journal.Int("port", c.Config.Port),
		)
		http.HandleFunc("/", c.createHandler(inChan))
		var err error
		if c.Config.EnableTLS {
			if c.Config.EnableLetsEncrypt {
				err = serveHTTPSWithLetsEncrypt(c.Config)
			} else {
				err = serveHTTPS(c.Config)
			}
		} else {
			err = serveHTTP(c.Config)
		}
		c.Logger.Error(
			"Web server ended",
			journal.Error(err),
		)
	}()

	return nil
}

// AcknowledgeMessage sets a message to successful if no error given, failed if an error is given.
func (c *Carrier) AcknowledgeMessage(message *flow.MessageContext, err error) error {
	c.Lock()
	_, ok := c.MessageLookup[message.GetTracingID()]
	c.Unlock()

	if !ok {
		return fmt.Errorf("%w: message not found", flow.ErrCarrierTransient)
	}

	if err == nil {
		c.Lock()
		c.MessageLookup[message.GetTracingID()] = MessageSuccess
		c.Unlock()
	} else {
		c.Lock()
		c.MessageLookup[message.GetTracingID()] = MessageFailed
		c.Unlock()
	}

	return nil
}

// InternalError sets a message to failed.
func (c *Carrier) InternalError(message *flow.MessageContext, err error) error {
	c.Lock()
	_, ok := c.MessageLookup[message.GetTracingID()]
	c.Unlock()

	if !ok {
		return fmt.Errorf("%w: message not found", flow.ErrCarrierTransient)
	}

	c.Lock()
	c.MessageLookup[message.GetTracingID()] = MessageFailed
	c.Unlock()

	return nil
}

// checkListenAddress checks if the listen address is valid.
func checkListenAddress(ip string) bool {
	// Special case for empty.
	if ip == "" {
		return true
	}

	if net.ParseIP(ip) == nil {
		return false
	}
	return true
}

// checkCertConf checks if all certificate config is setup.
func checkCertConf(config *Config) error {
	if len(config.Hostnames) < 1 {
		return fmt.Errorf("no hostnames set")
	}

	if _, err := os.Stat(config.CertDirectory); err != nil {
		return fmt.Errorf("problem accessing %s: %w", config.CertDirectory, err)
	}

	if config.EnableLetsEncrypt { //nolint:nestif
		if err := syscall.Access(config.CertDirectory, syscall.O_RDWR); err != nil {
			return fmt.Errorf("can't write LetsEncrypt certs to %s: %w", config.CertDirectory, err)
		}
	} else {
		if config.CertFiles.Certificate == "" || config.CertFiles.Key == "" {
			return fmt.Errorf("need both Certificate and Key defined if not using LetsEncrypt")
		}

		certPath := filepath.Join(config.CertDirectory, config.CertFiles.Certificate)
		keyPath := filepath.Join(config.CertDirectory, config.CertFiles.Key)
		if _, err := os.Stat(certPath); err != nil {
			return fmt.Errorf("problem accessing %s: %w", certPath, err)
		}
		if _, err := os.Stat(keyPath); err != nil {
			return fmt.Errorf("kroblem accessing %s: %w", certPath, err)
		}
	}

	return nil
}

func serveHTTP(c *Config) error {
	return http.ListenAndServe(
		fmt.Sprintf("%s:%d", c.ListenAddress, c.Port),
		nil,
	)
}

func serveHTTPS(c *Config) error {
	certPath := filepath.Join(c.CertDirectory, c.CertFiles.Certificate)
	keyPath := filepath.Join(c.CertDirectory, c.CertFiles.Key)
	return http.ListenAndServeTLS(
		fmt.Sprintf("%s:%d", c.ListenAddress, c.Port),
		certPath,
		keyPath,
		nil,
	)
}

func serveHTTPSWithLetsEncrypt(c *Config) error {
	certManager := autocert.Manager{
		Prompt:     autocert.AcceptTOS,
		HostPolicy: autocert.HostWhitelist(c.Hostnames...),
		Cache:      autocert.DirCache(c.CertDirectory),
	}

	server := &http.Server{
		Addr: fmt.Sprintf("%s:%d", c.ListenAddress, c.Port),
		TLSConfig: &tls.Config{
			GetCertificate: certManager.GetCertificate,
			MinVersion:     tls.VersionTLS12,
		},
	}

	go func() {
		err := http.ListenAndServe(":http", certManager.HTTPHandler(nil))
		if err != nil {
			panic("ACME server stopped " + err.Error())
		}
	}()

	return server.ListenAndServeTLS("", "")
}
