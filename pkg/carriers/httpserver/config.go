/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package httpserver

// CertFiles contain the names of the certificate and the key of the certificates to use.
type CertFiles struct {
	// Certificate is the name of the certificate file.
	Certificate string
	// Key is the name of the key file.
	Key string
}

// BasicAuth is a struct that holds basic auth information.
type BasicAuth struct {
	// Username is the username for basic auth.
	Username string
	// Password is the password for basic auth.
	Password string
	// Realm is the realm for basic auth.
	Realm string
}

// Config is the httpserver configuration struct.
type Config struct {
	// ListenAddress -- The address to listen on.
	// Default: 0.0.0.0
	ListenAddress string
	// Port -- The port to listen on, only used when TLS is not enabled
	// Default: 80 when TLS is disabled, 443 when TLS is enabled.
	Port int
	// EnableTLS -- Enable TLS
	// Default: false
	EnableTLS bool
	// CertDirectory -- The directory certificate files live in, or where LetsEncrypt writes its
	// certificate files.
	// Required if TLS enabled.
	CertDirectory string
	// Hostnames -- the hostnames to use for TLS
	// Default empty, required for TLS.
	Hostnames []string
	// EnableLetsEncrypt -- Acquire TLS certificate from LetsEncrypt
	// Default false
	EnableLetsEncrypt bool
	// CertFiles -- The name of the certificate/key files to use.
	// This will be ignored if EnableLetsEncrypt is set to true.
	CertFiles CertFiles
	// BasicAuth -- A basic auth struct, if nil, no basic auth will be setup.
	BasicAuth *BasicAuth
}
