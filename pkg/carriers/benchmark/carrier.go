package benchmark

import (
	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
)

// Carrier represents the debug carrier. For now, it doesn't do much except write messages to
// the inChan in a tight loop, and immediately return for all the other methods, but we might
// want to add tuneables in the future.
type Carrier struct {
	logger journal.Logger
}

func New(l journal.Logger) *Carrier {
	return &Carrier{
		logger: l.WithCopy(journal.String("input-carrier", "benchmark")),
	}
}

func (c *Carrier) insertMessages(inChan chan *flow.MessageContext) {
	for {
		inChan <- flow.NewMessageContext(
			flow.NewWrapper([]byte{}, "NOOP", "BENCHMARK"),
			c.logger,
			"NOOP",
			nil,
		)
	}
}

func (c *Carrier) SetupInput(inChan chan *flow.MessageContext) error {
	go c.insertMessages(inChan)
	return nil
}

func (c *Carrier) AcknowledgeMessage(m *flow.MessageContext, mErr error) error {
	return nil
}

func (c *Carrier) InternalError(m *flow.MessageContext, mErr error) error {
	return nil
}

func (c *Carrier) WriteOutput(message *flow.MessageContext) error {
	return nil
}

func (c *Carrier) ValidErrorCarrier() error {
	return nil
}

func (c *Carrier) WriteErr(message *flow.MessageContext) error {
	return nil
}

func (c *Carrier) WriteFail(message *flow.MessageContext) error {
	return nil
}

func (c *Carrier) PostProcess(outputError error, message *flow.MessageContext) error {
	return nil
}
