/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package kafka

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"os"
	"sync"

	"github.com/google/uuid"
	"github.com/segmentio/kafka-go"
	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
)

// messageLedger keeps track of messages to refer to in the acknowledge phase.
type messageLedger struct {
	sync.Mutex

	t map[uuid.UUID]*kafka.Message
}

func newLedger() *messageLedger {
	return &messageLedger{t: make(map[uuid.UUID]*kafka.Message)}
}

func (ml *messageLedger) AddMessage(id uuid.UUID, message *kafka.Message) {
	ml.Lock()
	if _, ok := ml.t[id]; ok {
		// This should not happen, and if it does, I want it to explode for now.
		panic(fmt.Sprintf("message %s already in ledger", id.String()))
	}
	ml.t[id] = message
	ml.Unlock()
}

func (ml *messageLedger) GetMessage(id uuid.UUID) (*kafka.Message, error) {
	ml.Lock()
	m, ok := ml.t[id]
	ml.Unlock()

	if !ok {
		return nil, fmt.Errorf("message %s not found", id)
	}

	return m, nil
}

func (ml *messageLedger) RemoveMessage(id uuid.UUID) {
	ml.Lock()
	delete(ml.t, id)
	ml.Unlock()
}

// Carrier signifies a Kafka carrier for the meander flow framework.
type Carrier struct {
	Config        *Config
	Reader        *kafka.Reader
	ErrReader     *kafka.Reader
	Writer        *kafka.Writer
	ErrWriter     *kafka.Writer
	FailWriter    *kafka.Writer
	Logger        journal.Logger
	Context       context.Context
	messageLedger *messageLedger
}

// New constructs a new Kafka carrier instance based on the config.
func New(ctx context.Context, logger journal.Logger, config *Config) (*Carrier, error) { //nolint:cyclop
	errTopic := fmt.Sprintf("%s-error", config.InputTopic)
	if config.ErrorTopic != "" {
		errTopic = config.ErrorTopic
	}
	if config.ErrorStrategy == ErrorStrategyRequeue {
		errTopic = config.InputTopic
	}

	// Define TLS configuration
	dialer, err := getDialer(config)
	if err != nil {
		return nil, err
	}

	logger.With(journal.Strings("brokers", config.Brokers))
	c := &Carrier{Config: config, Logger: logger, Context: ctx, messageLedger: newLedger()}
	if config.InputTopic != "" {
		if config.ConsumerGroup == "" {
			return nil, fmt.Errorf("%w: InputTopic requires ConsumerGroup", flow.ErrCarrierMisconfigured)
		}

		c.Logger.With(journal.String("consumer-group", config.ConsumerGroup))
		if config.ErrorTopicProcessing != ErrorTopicOnly {
			c.Logger.With(journal.String("input-topic", config.InputTopic))
			c.Reader = newReader(dialer, config.Brokers, config.InputTopic, config.ConsumerGroup)
		}
	}

	if config.OutputTopic != "" {
		c.Logger.With(journal.String("output-topic", config.OutputTopic))
		c.Writer = newWriter(dialer, config.Brokers, config.OutputTopic)
	}

	if err := setupErrorTopic(config, c, dialer, errTopic); err != nil {
		return nil, err
	}

	if (config.ErrorTopicProcessing != ErrorTopicOff) &&
		(config.ErrorStrategy != ErrorStrategyRequeue && config.ErrorTopicProcessing != ErrorTopicOnly) {
		c.Logger.With(journal.String("error-reader-topic", errTopic), journal.Bool("error-topic-processing", true))
		c.ErrReader = newReader(dialer, config.Brokers, errTopic, config.ConsumerGroup)
	}

	if config.FailStrategy == FailMove {
		failTopic := "failures"
		if config.FailTopic != "" {
			failTopic = config.FailTopic
		}
		c.Logger.With(journal.String("fail-strategy", "move"), journal.String("fail-topic", failTopic))
		c.FailWriter = newWriter(dialer, config.Brokers, failTopic)
	}

	c.Logger.Info("Configuration complete")
	return c, nil
}

func setupErrorTopic(config *Config, c *Carrier, dialer *kafka.Dialer, errTopic string) error {
	switch config.ErrorStrategy {
	case ErrorStrategyRequeue:
		if c.Reader == nil {
			return fmt.Errorf(
				"%w: requeue error strategy not supported if no input topic defined",
				flow.ErrCarrierMisconfigured,
			)
		}
		c.Logger.With(journal.String("error-topic", config.InputTopic), journal.String("error-strategy", "requeue"))
		c.ErrWriter = newWriter(dialer, config.Brokers, config.InputTopic)
	case ErrorStrategySeparate:
		c.Logger.With(journal.String("error-topic", errTopic), journal.String("error-strategy", "separate"))
		c.ErrWriter = newWriter(dialer, config.Brokers, errTopic)
	case ErrorStrategyDisable:
		c.Logger.With(journal.String("error-strategy", "disabled"))
		c.ErrWriter = nil
	}
	return nil
}

func getDialer(config *Config) (*kafka.Dialer, error) {
	dialer := kafka.DefaultDialer
	if config.CACertificateFilename == "" && config.ServiceKeyFilename == "" && config.ServiceCertificateFilename == "" {
		return dialer, nil
	}

	caCertData, err := os.ReadFile(config.CACertificateFilename)
	if err != nil {
		return nil, fmt.Errorf("%w: Failed reading CA certificate", flow.ErrCarrierMisconfigured)
	}

	serviceKeyData, err := os.ReadFile(config.ServiceKeyFilename)
	if err != nil {
		return nil, fmt.Errorf("%w: Failed reading service key", flow.ErrCarrierMisconfigured)
	}

	serviceCertData, err := os.ReadFile(config.ServiceCertificateFilename)
	if err != nil {
		return nil, fmt.Errorf("%w: Failed reading service certificate", flow.ErrCarrierMisconfigured)
	}

	certificate, err := tls.X509KeyPair(serviceCertData, serviceKeyData)
	if err != nil {
		return nil, fmt.Errorf("%w: Could not create X509 key pair", flow.ErrCarrierMisconfigured)
	}

	caCertPool := x509.NewCertPool()
	if ok := caCertPool.AppendCertsFromPEM(caCertData); !ok {
		return nil, fmt.Errorf("%w: Could not add CA cert to caCertPool", flow.ErrCarrierMisconfigured)
	}

	dialer = &kafka.Dialer{
		TLS: &tls.Config{
			Certificates:       []tls.Certificate{certificate},
			RootCAs:            caCertPool,
			InsecureSkipVerify: config.InsecureSkipVerify, //nolint:gosec
		},
	}
	return dialer, nil
}

// Close closes all kafka handles.
func (c *Carrier) Close() error {
	errs := make([]error, 0)
	if c.Reader != nil {
		errs = append(errs, c.Reader.Close())
	}
	if c.ErrReader != nil {
		errs = append(errs, c.ErrReader.Close())
	}
	if c.Writer != nil {
		errs = append(errs, c.Writer.Close())
	}
	if c.ErrWriter != nil {
		errs = append(errs, c.ErrWriter.Close())
	}
	if c.FailWriter != nil {
		errs = append(errs, c.FailWriter.Close())
	}

	errClose := errors.New("couldn't close some kafka handles")
	errFound := false
	for _, err := range errs {
		if err != nil {
			errClose = fmt.Errorf("%w: %s", errClose, err)
			errFound = true
		}
	}
	if errFound {
		return errClose
	}

	return nil
}
