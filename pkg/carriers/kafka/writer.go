/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package kafka

import (
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/segmentio/kafka-go"
	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
)

func newWriter(dialer *kafka.Dialer, brokers []string, topic string) *kafka.Writer {
	return kafka.NewWriter(kafka.WriterConfig{
		Dialer:    dialer,
		Brokers:   brokers,
		Topic:     topic,
		BatchSize: 1,
	})
}

// WriteOutput writes a message to the main writer.
func (c *Carrier) WriteOutput(m *flow.MessageContext) error {
	if c.Writer == nil {
		return fmt.Errorf("%w: no output writer defined", flow.ErrCarrierMisconfigured)
	}
	writeStart := time.Now()
	o, err := m.GetOutputProto()
	if err != nil {
		m.Logger().Error("Could not get output protobuf", journal.Error(err))
		return fmt.Errorf("%w: could not get output protobuf: %s", flow.ErrCarrierFatal, err)
	}
	kafkaMessageID := uuid.New().String()
	m.Logger().With(journal.String("kafka-message-id", kafkaMessageID))

	m.Logger().Info("Writing message to output topic", journal.String("kafka-id", kafkaMessageID))
	err = c.Writer.WriteMessages(m.Context(), kafka.Message{Key: []byte(kafkaMessageID), Value: o})
	if err != nil {
		m.Logger().Error("Failed to write message to kafka", journal.Error(err))
		return fmt.Errorf("%w: failed to write message", err)
	}
	m.Logger().Debug("Wrote message to output topic", journal.Duration("kafka-write-duration", time.Since(writeStart)))
	return nil
}

// ValidErrorCarrier checks if we have an ErrWriter, and returns an err if not.
func (c *Carrier) ValidErrorCarrier() error {
	if c.ErrWriter == nil {
		return fmt.Errorf(
			"%w: no error writer defined on kafka error carrier, use another carrier or configure the error carrier",
			flow.ErrCarrierMisconfigured,
		)
	}
	return nil
}

// WriteErr writes a message to the main error writer.
func (c *Carrier) WriteErr(m *flow.MessageContext) error {
	if c.ErrWriter == nil {
		return fmt.Errorf("%w: no error writer defined", flow.ErrCarrierMisconfigured)
	}

	o, err := m.GetErrorProto()
	if err != nil {
		m.Logger().Error("Could not get error protobuf", journal.Error(err))
		return fmt.Errorf("%w: could not get error protobuf: %s", flow.ErrCarrierFatal, err)
	}
	kafkaMessageID := uuid.New().String()
	m.Logger().With(journal.String("kafka-message-id", kafkaMessageID))

	m.Logger().Warn("Writing message to error topic")
	err = c.ErrWriter.WriteMessages(c.Context, kafka.Message{Key: []byte(kafkaMessageID), Value: o})
	if err != nil {
		m.Logger().Error("Failed to write message to kafka")
		return fmt.Errorf("%w: failed to write message", err)
	}
	return nil
}

// WriteFail writes a message to the main failure writer.
func (c *Carrier) WriteFail(m *flow.MessageContext) error {
	switch c.Config.FailStrategy {
	case FailDrop:
		m.Logger().Warn("Dropping failure message")
		return nil
	case FailFatal:
		m.Logger().Fatal("Failure encountered, exiting")
	case FailMove:
		if c.FailWriter == nil {
			m.Logger().Error("No failure writer defined")
			return fmt.Errorf("%w: no failure writer defined", flow.ErrCarrierMisconfigured)
		}
		o, err := m.GetErrorProto()
		if err != nil {
			m.Logger().Error("Could not get error protobuf", journal.Error(err))
			return fmt.Errorf("%w: could not get error protobuf: %s", flow.ErrCarrierFatal, err)
		}
		kafkaMessageID := uuid.New().String()
		m.Logger().With(journal.String("kafka-message-id", kafkaMessageID))
		m.Logger().Warn("Writing message to failure topic")
		err = c.FailWriter.WriteMessages(m.Context(), kafka.Message{Key: []byte(kafkaMessageID), Value: o})
		if err != nil {
			return fmt.Errorf("%w: failed to write message", err)
		}
		return nil
	default:
		return fmt.Errorf(
			"%w: invalid failure strategy defined: %d",
			flow.ErrCarrierMisconfigured,
			c.Config.FailStrategy,
		)
	}

	return nil
}
