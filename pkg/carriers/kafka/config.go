/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package kafka

// ErrorStrategy signifies how we handle errors.
type ErrorStrategy uint8

const (
	// Requeue -- requeue messages in the configured InputTopic.
	ErrorStrategyRequeue ErrorStrategy = iota
	// Separate -- requeue messages in a separate topic. (Named $InputTopic-error).
	ErrorStrategySeparate
	// Disable -- disable error writer, will exit with an error if used as such.
	ErrorStrategyDisable
)

// ErrorTopicProcessing defines the error topic processing options.
type ErrorTopicProcessing uint8

const (
	// Off -- Not processing the error topic.
	ErrorTopicOff ErrorTopicProcessing = iota
	// Only -- Only processing the error topic.
	ErrorTopicOnly
	// Both -- processing both the input topic and the error topic.
	ErrorTopicBoth
)

type FailStrategy uint8

const (
	// Move -- Moves the message to FailTopic.
	FailMove FailStrategy = iota
	// Fatal -- Exit code without committing the message.
	FailFatal
	// Drop -- Silently drop it.
	FailDrop
)

// Config is the kafka configuration object.
type Config struct {
	// Brokers -- a list of the Kafka brokers to connect to, not optional.
	// Required
	Brokers []string
	// InputTopic -- the topic to get input from. If empty, will not receive input, and be called in a loop.
	// Default: empty
	InputTopic string
	// OutputTopic -- the topic to output to. If empty, will not send output.
	// Default: empty
	OutputTopic string
	// ErrorTopic -- the topic to write errors to when "Separate" strategy in use, defaults
	// to $input-topic-error if not set
	ErrorTopic string
	// ErrorStrategy -- the error handling strategy to use.
	// Default: Requeue
	ErrorStrategy ErrorStrategy
	// ErrorTopicProcessing -- does this plugin process the error topic. Only relevant for the "Separate"
	// ErrorStrategy.
	// Default: "Off"
	ErrorTopicProcessing
	// FailStrategy -- The strategy to use once the RetryLimit has been reached.
	// Default: Move
	FailStrategy FailStrategy
	// FailTopic is the topic that messages that have been branded as a hard fault will be moved to.
	// Default: "failures"
	FailTopic string
	// ConsumerGroup -- The consumer group to use
	// Required if InputTopic set
	ConsumerGroup string
	// CACertificateFilename is the file containing the CA certificate in PEM format.
	CACertificateFilename string
	// ServiceKeyFilename is the file containing the client's service key in PEM format.
	ServiceKeyFilename string
	// ServiceCertificateFilename is the file containing the client's service key in PEM format.
	ServiceCertificateFilename string
	// InsecureSkipVerify controls whether a client verifies the server's certificate chain and host name.
	InsecureSkipVerify bool
}
