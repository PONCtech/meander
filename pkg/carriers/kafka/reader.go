/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package kafka

import (
	"fmt"

	"github.com/segmentio/kafka-go"
	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
)

func newReader(dialer *kafka.Dialer, brokers []string, topic, consumerGroup string) *kafka.Reader {
	return kafka.NewReader(kafka.ReaderConfig{
		Dialer:  dialer,
		Brokers: brokers,
		Topic:   topic,
		GroupID: consumerGroup,
	})
}

// SetupInput sets up go routines that write messages to inChan.
func (c *Carrier) SetupInput(inChan chan *flow.MessageContext) error {
	if c.Reader != nil {
		go monitorTopic(c, c.Reader, inChan)
	}

	if c.ErrReader != nil {
		go monitorTopic(c, c.ErrReader, inChan)
	}

	return nil
}

func monitorTopic(c *Carrier, topicReader *kafka.Reader, inChan chan *flow.MessageContext) {
	logger := c.Logger.WithCopy(
		journal.String("message-source", "kafka"),
		journal.String("topic", topicReader.Config().Topic),
	)
	logger.Info("Listening to kafka topic")
	for {
		message, err := topicReader.FetchMessage(c.Context)
		if err != nil {
			logger.Error("failed to receive message", journal.Error(err))
			continue
		}
		logger.Info("Received message from  topic.", journal.String("kafka-message-id", string(message.Key)))

		mCTX, err := flow.MessageContextFromWrapperProto(message.Value, logger, nil)
		if err != nil {
			logger.Error("Malformed message", journal.Error(err))
			continue
		}

		c.messageLedger.AddMessage(mCTX.GetLedgerID(), &message)

		inChan <- mCTX
	}
}

// AcknowledgeMessage commits the message, signaling that we processed it.
func (c *Carrier) AcknowledgeMessage(m *flow.MessageContext, mErr error) error {
	message, err := c.messageLedger.GetMessage(m.GetLedgerID())
	if err != nil {
		m.Logger().Error("Could not get message from ledger", journal.Error(err))
		return fmt.Errorf("%w: %s", flow.ErrCarrierTransient, err)
	}

	if c.ErrReader != nil && c.ErrReader.Config().Topic == message.Topic {
		return commitMessage(c, c.ErrReader, message, m)
	}

	return commitMessage(c, c.Reader, message, m)
}

func commitMessage(c *Carrier, reader *kafka.Reader, message *kafka.Message, m *flow.MessageContext) error {
	err := reader.CommitMessages(c.Context, *message)
	if err != nil {
		m.Logger().Error("Could not acknowledge error message", journal.Error(err))
		return fmt.Errorf("%w: could not acknowledge error message: %s", flow.ErrCarrierTransient, err)
	}

	m.Logger().Info(fmt.Sprintf("Message committed on topic %s", reader.Config().Topic))
	c.messageLedger.RemoveMessage(m.GetLedgerID())
	return nil
}

// InternalError is a noop for this carrier, as we just want to keep these in a queue.
func (c *Carrier) InternalError(m *flow.MessageContext, mErr error) error {
	return nil
}
