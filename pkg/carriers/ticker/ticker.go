/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package ticker

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
)

// Carrier defines the ticker carrier.
type Carrier struct {
	Context  context.Context
	Logger   journal.Logger
	Interval time.Duration
}

// New initialises a new ticker carrier.
func New(ctx context.Context, logger journal.Logger, config *Config) (*Carrier, error) {
	if config.Interval == 0 {
		return nil, fmt.Errorf("%w: Interval is required ", flow.ErrCarrierMisconfigured)
	}

	return &Carrier{
		Context:  ctx,
		Logger:   logger.WithCopy(journal.Duration("interval", config.Interval)),
		Interval: config.Interval,
	}, nil
}

// SetupInput creates a ticker and writes to inChan on every tick.
func (c *Carrier) SetupInput(inChan chan *flow.MessageContext) error {
	if c.Interval == 0 {
		return fmt.Errorf("%w: Interval is required ", flow.ErrCarrierMisconfigured)
	}

	c.Logger.Info("Initialising ticker")
	ticker := time.NewTicker(c.Interval)

	go func() {
		for {
			t := <-ticker.C
			c.Logger.Info("Tick received")
			inChan <- flow.NewMessageContext(
				flow.NewWrapper([]byte(fmt.Sprintf("Tick at %s", t)), "TICK", "TICKER"),
				c.Logger.Clone(),
				"TICK",
				nil,
			)
		}
	}()

	return nil
}

// AcknowledgeMessage is not relevant for this carrier.
func (c *Carrier) AcknowledgeMessage(m *flow.MessageContext, mErr error) error {
	return nil
}

// InternalError is not relevant for this carrier.
func (c *Carrier) InternalError(m *flow.MessageContext, mErr error) error {
	return nil
}
