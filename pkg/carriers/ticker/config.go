/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

// Package ticker offers an input carrier for the meander flow framework.
package ticker

import "time"

type Config struct {
	// Interval -- The interval of the ticker.
	Interval time.Duration
}
