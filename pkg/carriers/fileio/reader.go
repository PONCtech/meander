/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package fileio

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"path"

	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
	"google.golang.org/protobuf/proto"
)

// New initialises a new file reader carrier.
func NewReader(ctx context.Context, logger journal.Logger, config *Config) (*Carrier, error) {
	if config.Filename == "" {
		return nil, fmt.Errorf("%w: Filename is required ", flow.ErrCarrierMisconfigured)
	}

	if _, err := os.Stat(config.Filename); err != nil {
		return nil, fmt.Errorf("%w: File does not exist ", flow.ErrCarrierFatal)
	}

	return &Carrier{
		Context: ctx,
		Logger:  logger,
		Config:  *config,
	}, nil
}

// SetupInput creates a file reader, reads the file and exits.
func (c *Carrier) SetupInput(inChan chan *flow.MessageContext) error {
	if c.Config.Filename == "" {
		return fmt.Errorf("%w: Filename is required ", flow.ErrCarrierMisconfigured)
	}

	logger := c.Logger.WithCopy(
		journal.String("input-filename", c.Config.Filename),
		journal.String("input-dirname", c.Config.DirectoryName),
	)

	logger.Info("Reading file")

	data, err := os.ReadFile(c.Config.Filename)
	if err != nil {
		return fmt.Errorf("%w: Failed reading file ", flow.ErrCarrierFatal)
	}

	fd := &flow.FileData{
		Filename: path.Base(c.Config.Filename),
		FileData: data,
		MimeType: http.DetectContentType(data),
	}

	fdPb, err := proto.Marshal(fd)
	if err != nil {
		logger.Fatal("failed to marshal file data", journal.Error(err))
	}

	inChan <- flow.NewMessageContext(
		flow.NewWrapper(fdPb, "fileData", fd.Filename),
		logger,
		"fileData",
		nil,
	)

	inChan <- flow.NewMessageContext(nil, logger, "STOP", &flow.MessageOptions{Stop: true})
	return nil
}

// AcknowledgeMessage is not relevant for this carrier.
func (c *Carrier) AcknowledgeMessage(m *flow.MessageContext, mErr error) error {
	return nil
}

// InternalError is not relevant for this carrier.
func (c *Carrier) InternalError(m *flow.MessageContext, mErr error) error {
	return nil
}
