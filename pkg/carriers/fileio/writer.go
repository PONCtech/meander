/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package fileio

import (
	"context"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
	"google.golang.org/protobuf/proto"
)

func NewWriter(ctx context.Context, logger journal.Logger, config *Config) (*Carrier, error) {
	if config.DirectoryName != "" {
		file, err := os.Open(config.DirectoryName)
		if err != nil {
			return nil, fmt.Errorf("%w: directory does not exist ", flow.ErrCarrierMisconfigured)
		}
		defer file.Close()

		fileInfo, err := file.Stat()
		if err != nil {
			return nil, fmt.Errorf("%w: could not get directory information ", flow.ErrCarrierMisconfigured)
		}

		if !fileInfo.IsDir() {
			return nil, fmt.Errorf("%w: not a directory ", flow.ErrCarrierMisconfigured)
		}
	}

	return &Carrier{
		Context: ctx,
		Logger:  logger,
		Config:  *config,
	}, nil
}

func (c *Carrier) WriteOutput(message *flow.MessageContext) error {
	message.Logger().With(
		journal.String("output-filename", c.Config.Filename),
		journal.String("output-dirname", c.Config.DirectoryName),
		journal.Int64("output-overwrite", int64(c.Config.OverwriteBehavior)),
	)

	fd := &flow.FileData{}
	if err := proto.Unmarshal(message.GetPayload(), fd); err != nil {
		return err
	}
	message.Logger().With(journal.String("mime-type", fd.MimeType))

	filename := filepath.Join(c.Config.DirectoryName, fd.Filename)
	_, err := os.Stat(filename)

	if err == nil {
		switch c.Config.OverwriteBehavior {
		case Noop:
			message.Logger().Info("File already exists, skipping write")
			return nil
		case Fail:
			message.Logger().Error("File already exists, skipping write")
			return fmt.Errorf("%w: File already exists ", flow.ErrCarrierTransient)
		case Overwrite:
			message.Logger().Info("File already exists, overwriting")
		}
	}

	message.Logger().Info("Writing file")

	permissions := 0o600
	err = os.WriteFile(filename, fd.FileData, fs.FileMode(permissions))
	if err != nil {
		message.Logger().Error("Error writing file", journal.Error(err))
		return fmt.Errorf("%w: Error writing file ", err)
	}

	return nil
}

func (c *Carrier) ValidErrorCarrier() error {
	return nil
}

func (c *Carrier) WriteErr(message *flow.MessageContext) error {
	return nil
}

func (c *Carrier) WriteFail(message *flow.MessageContext) error {
	return nil
}
