/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

// Package fileio offers file reader/writer carriers for the meander
// flow framework.
package fileio

import (
	"context"

	"gitlab.com/PONCtech/meander/pkg/journal"
)

type Config struct {
	Filename      string
	DirectoryName string
	OverwriteBehavior
}

// Carrier defines the file carrier.
type Carrier struct {
	Context context.Context
	Logger  journal.Logger
	Config  Config
}

type OverwriteBehavior int64

const (
	Noop OverwriteBehavior = iota
	Fail
	Overwrite
)
