/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package stdout

import (
	"context"
	"os"

	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
)

type Carrier struct {
	Context      context.Context
	Logger       journal.Logger
	Deserialiser func(w *flow.Wrapper) (out string)
}

func New(ctx context.Context, logger journal.Logger, config *Config) (*Carrier, error) {
	fn := WrapperPrinter
	if config.Deserialiser != nil {
		fn = config.Deserialiser
	}
	return &Carrier{
		Context:      ctx,
		Logger:       logger,
		Deserialiser: fn,
	}, nil
}

func (c *Carrier) WriteOutput(message *flow.MessageContext) error {
	content := c.Deserialiser(message.GetWrapper())
	message.Logger().Debug("Writing new message to output")
	os.Stdout.Write([]byte(content))
	return nil
}

func (c *Carrier) ValidErrorCarrier() error {
	return nil
}

func (c *Carrier) WriteErr(message *flow.MessageContext) error {
	content := c.Deserialiser(message.GetErrWrapper())
	c.Logger.Debug("Writing new message to error")
	os.Stderr.Write([]byte(content))
	return nil
}

func (c *Carrier) WriteFail(message *flow.MessageContext) error {
	content := c.Deserialiser(message.GetErrWrapper())
	c.Logger.Info(
		"Writing new message to fail",
	)
	os.Stderr.Write([]byte(content))
	return nil
}
