/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package stdout

import (
	"fmt"
	"strings"

	"gitlab.com/PONCtech/meander/pkg/flow"
)

// BytePrinter attempts to convert bytes to strings.
func BytePrinter(in []byte) (out string) {
	return string(in)
}

// WrapperPrinter attempts to deserialise a wrapper protobuf and returns a formatted
// overview.
func WrapperPrinter(w *flow.Wrapper) (out string) {
	return fmt.Sprintf(
		"UUID: %s\tCreationTime: %s\tVersion: %d\tErrors: %s\tOrigin: %s\tType: %s\tPayload: %s",
		w.Uuid,
		w.CreationTime.AsTime(),
		w.Version,
		strings.Join(w.Errors, ", "),
		w.Origin,
		w.Type,
		BytePrinter(w.Payload),
	)
}
