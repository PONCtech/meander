/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

// Package printer defines printer output and error carriers.
// These are mainly useful for debugging.
package stdout

import "gitlab.com/PONCtech/meander/pkg/flow"

// Config structure for the printer carriers.
type Config struct {
	// Deserialiser is the deserialisation function, it will get passed the byte array
	// that is being passed to the output and returns a string to be printed.
	// Defaults to BytePrinter
	Deserialiser func(w *flow.Wrapper) (out string)
}
