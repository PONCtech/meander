package journal

import (
	"fmt"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// ZapLogger is a wrapper for the zap logger to use was a meander logger.
type ZapLogger struct {
	l *zap.Logger
}

// NewZap takes a zap.Logger and returns a zapLogger wrapper.
func NewZap(l *zap.Logger) Logger {
	return &ZapLogger{l: l.WithOptions(zap.AddCallerSkip(1))}
}

// Debug logs a debug message with the current fields.
func (z *ZapLogger) Debug(message string, fields ...Field) {
	zf := convertFieldsToZap(fields...)
	z.l.Debug(message, zf...)
}

// Info logs an info message with the current fields.
func (z *ZapLogger) Info(message string, fields ...Field) {
	zf := convertFieldsToZap(fields...)
	z.l.Info(message, zf...)
}

// Warn logs a warn message with the current fields.
func (z *ZapLogger) Warn(message string, fields ...Field) {
	zf := convertFieldsToZap(fields...)
	z.l.Warn(message, zf...)
}

// Error logs an error message with the current fields.
func (z *ZapLogger) Error(message string, fields ...Field) {
	zf := convertFieldsToZap(fields...)
	z.l.Warn(message, zf...)
}

// Fatal logs a fatal message with the current fields.
func (z *ZapLogger) Fatal(message string, fields ...Field) {
	zf := convertFieldsToZap(fields...)
	z.l.Warn(message, zf...)
}

// With adds the current fields to the logger.
func (z *ZapLogger) With(fields ...Field) {
	zf := convertFieldsToZap(fields...)
	z.l = z.l.With(zf...)
}

// With returns a copy of the current logger with the given fields added.
func (z *ZapLogger) WithCopy(fields ...Field) Logger {
	zf := convertFieldsToZap(fields...)
	return &ZapLogger{l: z.l.With(zf...)}
}

// Clone returns a copy of the logger in its current state.
func (z *ZapLogger) Clone() Logger {
	return &ZapLogger{l: z.l}
}

// AddCallerSkipCopy returns a logger with an amount of callers skipped.
func (z *ZapLogger) AddCallerSkipCopy(n int) Logger {
	return &ZapLogger{l: z.l.WithOptions(zap.AddCallerSkip(n))}
}

func convertFieldsToZap(fields ...Field) []zapcore.Field {
	zf := make([]zap.Field, 0)
	for _, f := range fields {
		zf = append(zf, convertFieldToZap(f))
	}
	return zf
}

func convertFieldToZap(field Field) zapcore.Field { //nolint:cyclop,funlen // This func will be long and repeating
	switch field.Type {
	case SkipType:
		return zap.Skip()
	case StringType:
		v, ok := field.Value.(string)
		checkOK(ok)
		return zap.String(field.Key, v)
	case StringsType:
		v, ok := field.Value.([]string)
		checkOK(ok)
		return zap.Strings(field.Key, v)
	case IntType:
		v, ok := field.Value.(int)
		checkOK(ok)
		return zap.Int(field.Key, v)
	case Int64Type:
		v, ok := field.Value.(int64)
		checkOK(ok)
		return zap.Int64(field.Key, v)
	case ErrorType:
		v, ok := field.Value.(error)
		checkOK(ok)
		return zap.Error(v)
	case BoolType:
		v, ok := field.Value.(bool)
		checkOK(ok)
		return zap.Bool(field.Key, v)
	case DurationType:
		v, ok := field.Value.(time.Duration)
		checkOK(ok)
		return zap.Duration(field.Key, v)
	case TimeType:
		v, ok := field.Value.(time.Time)
		checkOK(ok)
		return zap.Time(field.Key, v)
	case ByteStringType:
		v, ok := field.Value.([]byte)
		checkOK(ok)
		return zap.ByteString(field.Key, v)
	default:
		panic(fmt.Sprintf("Not a supported field type: %d", field.Type))
	}
}

func checkOK(ok bool) {
	if !ok {
		panic("Wrong type assertion")
	}
}
