// Package journal is a log abstraction package. For now only zap is supported.
package journal

// Logger defines the interface for a logger.
type Logger interface {
	// Info logs a message with the specified fields at the debug level.
	Debug(message string, fields ...Field)
	// Info logs a message with the specified fields at the info level.
	Info(message string, fields ...Field)
	// Warn logs a message with the specified fields at the warn level.
	Warn(message string, fields ...Field)
	// Error logs a message with the specified fields at the error level.
	Error(message string, fields ...Field)
	// Fatal logs a message with the specified fields at the fatal level, stopping the program.
	Fatal(message string, fields ...Field)
	// With adds the specified fields permanently, note you can't remove fields.
	With(fields ...Field)
	// WithCopy returns a copy of the logger with the specified fields added permanently, does not
	// affect the logger itself.
	WithCopy(fields ...Field) Logger
	// AddCallerSkipCopy returns a copy of the logger with a few layers of caller skip added.
	// Use this if you wrap the logger into an adapter.
	AddCallerSkipCopy(n int) Logger
	// Clone returns a copy of the current logger.
	Clone() Logger
}
