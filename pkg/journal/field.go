package journal

import (
	"fmt"
	"time"
)

// FieldType represents the type of field it is.
type FieldType uint8

// The types we support.
// TODO: Document these better.
const (
	StringType FieldType = iota
	StringsType
	IntType
	Int64Type
	ErrorType
	BoolType
	TimeType
	DurationType
	ByteStringType
	SkipType
)

// Field represents a log field.
type Field struct {
	Type  FieldType
	Key   string
	Value any
}

// String returns a string field.
func String(key string, value string) Field {
	return Field{
		Type:  StringType,
		Key:   key,
		Value: value,
	}
}

// Strings returns a strings field.
func Strings(key string, value []string) Field {
	return Field{
		Type:  StringsType,
		Key:   key,
		Value: value,
	}
}

// Int returns an int field.
func Int(key string, value int) Field {
	return Field{
		Type:  IntType,
		Key:   key,
		Value: value,
	}
}

// Int returns an int field.
func Int64(key string, value int64) Field {
	return Field{
		Type:  Int64Type,
		Key:   key,
		Value: value,
	}
}

// Error returns an error field.
func Error(value error) Field {
	if value == nil {
		return Skip()
	}
	return Field{
		Type:  ErrorType,
		Value: value,
	}
}

// Bool returns a bool field.
func Bool(key string, value bool) Field {
	return Field{
		Type:  BoolType,
		Key:   key,
		Value: value,
	}
}

// Duration returns a duration field.
func Duration(key string, value time.Duration) Field {
	return Field{
		Type:  DurationType,
		Key:   key,
		Value: value,
	}
}

// Time returns a time field.
func Time(key string, value time.Time) Field {
	return Field{
		Type:  TimeType,
		Key:   key,
		Value: value,
	}
}

// ByteString returns a byte string field.
func ByteString(key string, value []byte) Field {
	return Field{
		Type:  ByteStringType,
		Key:   key,
		Value: value,
	}
}

// Skip just omits a field.
func Skip() Field {
	return Field{
		Type: SkipType,
	}
}

// Any takes a key and arbitrary field and chooses the best way to represent them as a field.
// TODO: Incomplete, as we don't support nearly all the fields yet, we're falling back on
// a rough stringification for the ones we don't support yet.
func Any(key string, value interface{}) Field {
	switch val := value.(type) {
	case string:
		return String(key, val)
	case []string:
		return Strings(key, val)
	case int:
		return Int(key, val)
	case int64:
		return Int64(key, val)
	// TODO: Support NamedError here
	case error:
		return Error(val)
	case bool:
		return Bool(key, val)
	case time.Duration:
		return Duration(key, val)
	case time.Time:
		return Time(key, val)
	default:
		return String(key, fmt.Sprintf("%+v", val))
	}
}
