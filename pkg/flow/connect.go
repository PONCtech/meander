/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package flow

import (
	"errors"
	"fmt"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/PONCtech/meander/pkg/journal"
)

const MessageVersion = 1

const bufferSize = 100

// Prometheus stats.
var (
	inputMessages = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_input_messages",
		Help: "Total messages received from input carrier",
	})
	outputMessages = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_output_messages",
		Help: "Total messages sent to the output carrier",
	})
	stopSignals = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_stop_signals",
		Help: "Number of times plugins signaled that message was not relevant",
	})
	totalErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_total_errors",
		Help: "Total errors encountered, (so both transient and fatal)",
	})
	transientErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_transient_errors",
		Help: "Total transient errors encountered",
	})
	fatalErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_fatal_errors",
		Help: "Total fatal errors encountered",
	})
	errErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_error_errors",
		Help: "Total errors encountered while handling errors",
	})
	messagesAcknowledged = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_messages_acknowledged",
		Help: "Total amount of messages acknowledged to input carrier",
	})
	messagesInternalErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_messages_internal_errors",
		Help: "Total amount of messages sent to the internal error handler of input carrier",
	})
	outputErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "meander_flow_messages_output_errors",
		Help: "Total errors from output carrier",
	})
)

// Connect connects a series of plugins to the flow. It passes a channel to the input
// carrier, which it monitors for input. When a message is received, it unmarshals it
// into the meander flow struct and then passes the payload as the argument to the
// first plugin. It will pass the output of that plugin to the next and repeat that
// until either the last plugin returns or any plugin returns an error. It will
// then push the output to the output carrier, or, in case of an error it will push
// the original message to the error carrier.
func Connect(
	baseLogger journal.Logger,
	config *Config,
	ic InputCarrier,
	oc OutputCarrier,
	ec ErrorCarrier,
	pp PostProcessor,
	plugins []Plugin,
) error {
	inChan, err := setup(baseLogger, ic)
	if err != nil {
		return err
	}

	if err := ec.ValidErrorCarrier(); err != nil {
		return err
	}

	baseLogger.Info("Starting main loop")

	// Main loop.
	for {
		message := <-inChan

		if message.StopRequested() {
			message.Logger().Info("Received stop message toggle. Exiting...")
			break
		}
		inputMessages.Inc()

		message.Logger().Info("Processing message")
		err := runPlugins(message, plugins)

		// Check for the Stop error type, which will make us stop processing, ack the message, and
		// not report an error. This is needed to support "gate" plugins that check if we want to
		// process this message further in this flow.
		if checkStop(err, ic, message) {
			continue
		}
		if checkErr(err, config, ec, ic, message) {
			continue
		}

		err = handleOutput(oc, message)

		runPostProcessor(pp, err, message)

		if err != nil {
			// If output wasn't handled correctly, we will not ack the message.
			message.Logger().Error("error while handling output", journal.Error(err))
			outputErrors.Inc()
			errMessage(ic, message, err)
			continue
		}
		outputMessages.Inc()
		ackMessage(ic, message, nil)
	}
	return nil
}

func runPostProcessor(pp PostProcessor, err error, message *MessageContext) {
	if pp != nil {
		if err := pp.PostProcess(err, message); err != nil {
			message.Logger().Warn("failed running post-processor", journal.Error(err))
		}
	}
}

func checkErr(
	err error,
	config *Config,
	ec ErrorCarrier,
	ic InputCarrier,
	message *MessageContext,
) bool {
	if err != nil {
		// If we didn't handle the error correctly, we will not acknowledge the message.
		if hErr := handleError(config, ec, message, err); hErr != nil {
			errErrors.Inc()
			message.Logger().Error("error while handling error", journal.Error(err), journal.Error(hErr))
			errMessage(ic, message, err)
			return true
		}
		ackMessage(ic, message, err)
		return true
	}
	return false
}

func checkStop(err error, ic InputCarrier, message *MessageContext) bool {
	if errors.Is(err, ErrPluginStop) {
		stopSignals.Inc()
		message.Logger().Info("not handling message", journal.String("message", err.Error()))
		ackMessage(ic, message, nil)
		return true
	}
	return false
}

// runPlugins runs the plugins sequentially, feeding the output of the plugin to the
// input to the next. The last output gets return to the caller. If any error is
// encountered, the sequence stops and the error gets returned.
// Note that as we are working on a pointer, we change the content of the message without returning it.
func runPlugins(
	message *MessageContext,
	plugins []Plugin,
) error {
	for _, plugin := range plugins {
		err := plugin(message)
		if err != nil {
			message.Logger().Error("Plugin encountered error", journal.Error(err))
			return err
		}
	}

	return nil
}

func ackMessage(ic InputCarrier, message *MessageContext, mErr error) {
	if mErr != nil {
		message.Logger().Error("Acknowledging message with error", journal.Error(mErr))
	} else {
		message.Logger().Info("Acknowledging message")
	}
	messagesAcknowledged.Inc()
	if err := ic.AcknowledgeMessage(message, mErr); err != nil {
		message.Logger().Fatal("couldn't acknowledge message", journal.Error(err))
	}
}

func errMessage(ic InputCarrier, message *MessageContext, mErr error) {
	messagesInternalErrors.Inc()
	if err := ic.InternalError(message, mErr); err != nil {
		message.Logger().Fatal("couldn't notify carrier of internal error", journal.Error(err))
	}
}

func setup(logger journal.Logger, ic InputCarrier) (chan *MessageContext, error) {
	// Setting a buffer to the input channel as that will have the biggest input on performance.
	// It probably needs some tweaking in the future. Note that setting this
	// too high could also make a single instance slurp too many messages.
	inChan := make(chan *MessageContext, bufferSize)

	if err := ic.SetupInput(inChan); err != nil {
		logger.Error("couldn't connect input channel", journal.Error(err))
		return inChan, fmt.Errorf("%w: couldn't connect input channel", err)
	}

	return inChan, nil
}

// handleOutput creates a message that is designed to be written to the output plugin.
func handleOutput(oc OutputCarrier, message *MessageContext) error {
	message.Logger().Info("handling output")

	var err error
	start := time.Now()
	// Retry output writes so that a hiccup doesn't cause any problems.
	for i := 0; i < 10; i++ {
		if err = oc.WriteOutput(message); err == nil {
			message.Logger().Debug("Breaking for properly sent message")
			break
		}
		// TODO: handle marshal errors better, as now this will loop for 10 times no matter what.
		message.Logger().Debug("Sleeping for a second", journal.Error(err))
		time.Sleep(time.Second)
	}
	message.Logger().Debug("Output sent", journal.Duration("sending-duration", time.Since(start)))

	if err != nil {
		return err
	}

	return nil
}

// handleError checks if an error needs to be retried, rewraps it, and passes it to the right handler.
func handleError(
	config *Config,
	ec ErrorCarrier,
	message *MessageContext,
	pluginErr error,
) error {
	message.Logger().Error("handling error", journal.Error(pluginErr))
	totalErrors.Inc()
	errCount := message.AddError(pluginErr)
	message.Logger().With(
		journal.Int("errCount", errCount),
		journal.Int("RetryLimit", config.RetryLimit),
	)

	if errors.Is(pluginErr, ErrPluginFatal) || errCount > config.RetryLimit {
		fatalErrors.Inc()
		message.Logger().Error("message fatal error", journal.Error(pluginErr))

		// Retry output writes so that a hiccup doesn't cause any problems.
		var err error
		for i := 0; i < 10; i++ {
			if err = ec.WriteFail(message); err == nil {
				break
			}
			time.Sleep(time.Second)
		}

		if err != nil {
			return err
		}

		return nil
	}
	transientErrors.Inc()

	message.Logger().Error("message errCount under RetryLimit, writing normal error", journal.Error(pluginErr))
	// Retry output writes so that a hiccup doesn't cause any problems.
	var err error
	for i := 0; i < 10; i++ {
		if err = ec.WriteErr(message); err == nil {
			break
		}
		time.Sleep(time.Second)
	}
	if err != nil {
		return err
	}

	return nil
}
