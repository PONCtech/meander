/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package flow

// Plugin is the definition of a transformation plugin function. It takes the tracing ID from
// the wrapper, and the bytes of the payload. It returns the output of the payload and any error
// it encountered.
type Plugin func(i *MessageContext) (err error)

type ToggleState int64

const (
	Noop ToggleState = iota
	Stop
)

// InputCarrier is an interface for how input from the flow the plugin.
type InputCarrier interface {
	// SetupInput takes a channel and will write input to the channel.
	// Returns error on failure
	SetupInput(inChan chan *MessageContext) error

	// AcknowledgeMessage acknowledges a message, marking it as processed.
	// If passed error is not nil, it signals a failed but processed message.
	// Returns error when acknowledgement fails.
	AcknowledgeMessage(message *MessageContext, err error) error

	// InternalError signals the input carrier that an internal error has happened.
	// This is mostly here for interactive carriers, for others this will be a noop.
	InternalError(message *MessageContext, err error) error
}

// OutputCarrier is an interface for how output will be inserted in the flow.
type OutputCarrier interface {
	// WriteOutput writes a message to output
	WriteOutput(message *MessageContext) error
}

// ErrorCarrier is an interface for how errors will reach the flow.
type ErrorCarrier interface {
	// ValidErrorCarrier returns an error if not a valid error carrier.
	ValidErrorCarrier() error
	// WriteErr writes a message to the err channel.
	WriteErr(message *MessageContext) error

	// WriteFail writes a message to the fail channel.
	WriteFail(message *MessageContext) error
}

// The PostProcessor is used for things like sending statistics or other
// things that are not for the continued flow of messages.
type PostProcessor interface {
	// PostProcess is an additional "plugin" you can apply after handling output.
	PostProcess(outputError error, message *MessageContext) error
}
