//
//Copyright (c) 2022 PONC.tech
//
//Licensed under the MIT license: https://opensource.org/licenses/MIT
//Permission is granted to use, copy, modify, and redistribute the work.
//Full license information available in the project LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.6.1
// source: flow/file_data.proto

package flow

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type FileData struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Filename string `protobuf:"bytes,1,opt,name=filename,proto3" json:"filename,omitempty"`
	FileData []byte `protobuf:"bytes,2,opt,name=file_data,json=fileData,proto3" json:"file_data,omitempty"`
	MimeType string `protobuf:"bytes,3,opt,name=mime_type,json=mimeType,proto3" json:"mime_type,omitempty"`
}

func (x *FileData) Reset() {
	*x = FileData{}
	if protoimpl.UnsafeEnabled {
		mi := &file_flow_file_data_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FileData) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FileData) ProtoMessage() {}

func (x *FileData) ProtoReflect() protoreflect.Message {
	mi := &file_flow_file_data_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FileData.ProtoReflect.Descriptor instead.
func (*FileData) Descriptor() ([]byte, []int) {
	return file_flow_file_data_proto_rawDescGZIP(), []int{0}
}

func (x *FileData) GetFilename() string {
	if x != nil {
		return x.Filename
	}
	return ""
}

func (x *FileData) GetFileData() []byte {
	if x != nil {
		return x.FileData
	}
	return nil
}

func (x *FileData) GetMimeType() string {
	if x != nil {
		return x.MimeType
	}
	return ""
}

var File_flow_file_data_proto protoreflect.FileDescriptor

var file_flow_file_data_proto_rawDesc = []byte{
	0x0a, 0x14, 0x66, 0x6c, 0x6f, 0x77, 0x2f, 0x66, 0x69, 0x6c, 0x65, 0x5f, 0x64, 0x61, 0x74, 0x61,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x04, 0x66, 0x6c, 0x6f, 0x77, 0x22, 0x60, 0x0a, 0x08,
	0x46, 0x69, 0x6c, 0x65, 0x44, 0x61, 0x74, 0x61, 0x12, 0x1a, 0x0a, 0x08, 0x66, 0x69, 0x6c, 0x65,
	0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x66, 0x69, 0x6c, 0x65,
	0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x66, 0x69, 0x6c, 0x65, 0x5f, 0x64, 0x61, 0x74,
	0x61, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0c, 0x52, 0x08, 0x66, 0x69, 0x6c, 0x65, 0x44, 0x61, 0x74,
	0x61, 0x12, 0x1b, 0x0a, 0x09, 0x6d, 0x69, 0x6d, 0x65, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x6d, 0x69, 0x6d, 0x65, 0x54, 0x79, 0x70, 0x65, 0x42, 0x26,
	0x5a, 0x24, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x50, 0x4f, 0x4e,
	0x43, 0x74, 0x65, 0x63, 0x68, 0x2f, 0x6d, 0x65, 0x61, 0x6e, 0x64, 0x65, 0x72, 0x2f, 0x70, 0x6b,
	0x67, 0x2f, 0x66, 0x6c, 0x6f, 0x77, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_flow_file_data_proto_rawDescOnce sync.Once
	file_flow_file_data_proto_rawDescData = file_flow_file_data_proto_rawDesc
)

func file_flow_file_data_proto_rawDescGZIP() []byte {
	file_flow_file_data_proto_rawDescOnce.Do(func() {
		file_flow_file_data_proto_rawDescData = protoimpl.X.CompressGZIP(file_flow_file_data_proto_rawDescData)
	})
	return file_flow_file_data_proto_rawDescData
}

var file_flow_file_data_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_flow_file_data_proto_goTypes = []interface{}{
	(*FileData)(nil), // 0: flow.FileData
}
var file_flow_file_data_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_flow_file_data_proto_init() }
func file_flow_file_data_proto_init() {
	if File_flow_file_data_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_flow_file_data_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FileData); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_flow_file_data_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_flow_file_data_proto_goTypes,
		DependencyIndexes: file_flow_file_data_proto_depIdxs,
		MessageInfos:      file_flow_file_data_proto_msgTypes,
	}.Build()
	File_flow_file_data_proto = out.File
	file_flow_file_data_proto_rawDesc = nil
	file_flow_file_data_proto_goTypes = nil
	file_flow_file_data_proto_depIdxs = nil
}
