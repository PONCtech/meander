/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package flow

import "errors"

var (
	ErrPluginMisconfigured = errors.New("plugin misconfigured")
	ErrPluginFatal         = errors.New("fatal plugin error")
	ErrPluginTransient     = errors.New("transient plugin error")
	ErrPluginStop          = errors.New("not handling input, stop processing")

	ErrCarrierMisconfigured = errors.New("carrier misconfigured")
	ErrCarrierFatal         = errors.New("fatal carrier error")
	ErrCarrierTransient     = errors.New("transient carrier error")
)
