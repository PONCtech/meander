/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package flow

type Config struct {
	// RetryLimit -- The amount of times we'll retry before it gets branded a hard fault.
	// Set this to 0 to disable retrying.
	// Default: 10
	RetryLimit int
}
