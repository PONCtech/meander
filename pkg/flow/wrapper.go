package flow

import (
	"github.com/google/uuid"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// NewWrapper returns contents wrapped, with the payload, type, and origin set as defined,
// but with a newly generated UUID.
func NewWrapper(contents []byte, msgType, msgOrigin string) *Wrapper {
	return NewWrapperWithTracingID(contents, msgType, msgOrigin, uuid.New())
}

// NewWrapperWithTracingID returns a new wrapper, with the payload, type, origin, and ID set as defined.
func NewWrapperWithTracingID(contents []byte, msgType, msgOrigin string, tracingID uuid.UUID) *Wrapper {
	return &Wrapper{
		Version:      MessageVersion,
		Payload:      contents,
		CreationTime: timestamppb.Now(),
		Uuid:         tracingID.String(),
		Type:         msgType,
		Origin:       msgOrigin,
	}
}

// GetID gets the tracing ID from the wrapper.
func (w *Wrapper) GetID() uuid.UUID {
	if w.Uuid != "" {
		return uuid.MustParse(w.Uuid)
	}
	panic("No ID set for this message.")
}
