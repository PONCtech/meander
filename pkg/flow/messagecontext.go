package flow

import (
	"context"
	"os"

	"github.com/google/uuid"
	"gitlab.com/PONCtech/meander/pkg/journal"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// MessageOptions are options set for this message.
type MessageOptions struct {
	// Stop will request a stop of the program.
	Stop bool
}

// MessageContext contains the context of the message, this includes the actual raw message,
// the ID used inside the carrier used for bookkeeping, state control, a logger with tags, and
// a context object.
type MessageContext struct {
	LedgerID    uuid.UUID
	wrapper     *Wrapper
	ctx         context.Context
	logger      journal.Logger
	options     *MessageOptions
	origWrapper *Wrapper
	messageType string

	// TODO: Make this more flexible.
	postProcessData map[string]string
}

// NewMessageContext takes a wrapper, a logger, and a string for messageType, and then returns
// a full MessageContext structure, including a cloned wrapper for error handling.
func NewMessageContext(w *Wrapper, l journal.Logger, messageType string, options *MessageOptions) *MessageContext {
	o, ok := proto.Clone(w).(*Wrapper)
	if !ok {
		panic("failed to assert type clone wrapper")
	}
	return &MessageContext{
		LedgerID:    uuid.New(),
		wrapper:     w,
		logger:      l.WithCopy(journal.String("tracing-id", w.GetUuid())),
		ctx:         context.Background(),
		options:     options,
		origWrapper: o,
		messageType: messageType,
	}
}

// MessageContextFromWrapperProto takes a binary wrapper protobuffer, unpacks it, populates a
// MessageContext with it, and returns it.
func MessageContextFromWrapperProto(
	binProto []byte,
	l journal.Logger,
	options *MessageOptions,
) (*MessageContext, error) {
	w := &Wrapper{}
	if err := proto.Unmarshal(binProto, w); err != nil {
		return nil, err
	}
	return NewMessageContext(w, l, w.GetType(), options), nil
}

// GetWrapperProto returns a protobuf marshalled version of message wrapper.
func (m *MessageContext) GetWrapperProto() ([]byte, error) {
	return proto.Marshal(m.wrapper)
}

// GetOutputProto alters the wrapper's fields to be sent out as output, and returns the protobuffer.
func (m *MessageContext) GetOutputProto() ([]byte, error) {
	m.wrapper.CreationTime = timestamppb.Now()
	m.wrapper.Origin = getHostname()
	m.wrapper.Type = m.messageType
	return proto.Marshal(m.wrapper)
}

// AddError adds an error to the origWrapper and returns the error count.
func (m *MessageContext) AddError(e error) int {
	m.origWrapper.Errors = append(m.origWrapper.Errors, e.Error())
	return len(m.origWrapper.Errors)
}

// GetErrorProto changes the origin on the original wrapper and returns the protobuffer.
func (m *MessageContext) GetErrorProto() ([]byte, error) {
	m.origWrapper.Origin = getHostname()
	return proto.Marshal(m.origWrapper)
}

// StopRequested checks if a stop is requested.
func (m *MessageContext) StopRequested() bool {
	if m.options == nil {
		return false
	}

	return m.options.Stop
}

// GetWrapper returns the wrapper.
func (m *MessageContext) GetWrapper() *Wrapper {
	return m.wrapper
}

// GetErrWrapper returns the origWrapper.
func (m *MessageContext) GetErrWrapper() *Wrapper {
	return m.origWrapper
}

// GetMessageType returns the set message type.
func (m *MessageContext) GetMessageType() string {
	return m.messageType
}

// SetMessageType sets the message type.
func (m *MessageContext) SetMessageType(t string) {
	m.messageType = t
}

func (m *MessageContext) Logger() journal.Logger {
	if m.logger == nil {
		// TODO: Write a null logger to use here.
		panic("message context not initialised properly")
	}
	return m.logger
}

func (m *MessageContext) Context() context.Context {
	if m.ctx == nil {
		return context.Background()
	}

	return m.ctx
}

func (m *MessageContext) GetPayload() []byte {
	if m.wrapper == nil {
		panic("wrapper not set yet")
	}
	return m.wrapper.Payload
}

func (m *MessageContext) SetPayload(p []byte) {
	if m.wrapper == nil {
		panic("wrapper not set yet")
	}
	m.wrapper.Payload = p
}

func (m *MessageContext) GetTracingID() uuid.UUID {
	if m.wrapper == nil {
		panic("wrapper not set yet")
	}

	return m.wrapper.GetID()
}

func (m *MessageContext) GetLedgerID() uuid.UUID {
	// Get empty uuid
	var e uuid.UUID
	if m.LedgerID == e {
		m.LedgerID = uuid.New()
	}
	return m.LedgerID
}

func (m *MessageContext) GetPostProcessData() map[string]string {
	if m.postProcessData == nil {
		return make(map[string]string)
	}
	return m.postProcessData
}

func (m *MessageContext) SetPostProcessValue(key, value string) {
	if m.postProcessData == nil {
		m.postProcessData = make(map[string]string)
	}
	m.postProcessData[key] = value
}

func getHostname() string {
	hostname, err := os.Hostname()
	if err != nil {
		// TODO: detect IP if hostname failed
		hostname = "undetected"
	}
	return hostname
}
