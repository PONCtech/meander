/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package noop

import "gitlab.com/PONCtech/meander/pkg/flow"

// Plugin is the echo plugin, a simple example plugin.
func Plugin(i *flow.MessageContext) (err error) {
	return nil
}

type PostProcessor int

func (pp *PostProcessor) PostProcess(outputError error, message *flow.MessageContext) error {
	return nil
}
