/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

package noop_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
	"gitlab.com/PONCtech/meander/pkg/plugins/noop"
	"go.uber.org/zap"
)

func TestPlugin(t *testing.T) {
	type args struct {
		in []byte
	}
	tests := []struct {
		name    string
		args    args
		wantOut []byte
		wantErr bool
	}{
		{
			name: "Simple echo test",
			args: args{
				in: []byte("test"),
			},
			wantOut: []byte("test"),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wrapper := flow.Wrapper{}
			wrapper.Payload = tt.args.in
			message := flow.NewMessageContext(
				flow.NewWrapper(tt.args.in, "TEST", "TEST"),
				journal.NewZap(zap.NewExample()),
				"TEST",
				nil,
			)
			err := noop.Plugin(message)
			if tt.wantErr {
				assert.Error(t, err)
			}
			assert.Equal(t, message.GetPayload(), tt.wantOut)
		})
	}
}
