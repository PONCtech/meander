/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

// This is a small demo of the most simple meander flow. It initialises a ticker
// input carrier, a printer output/error carrier, and uses the noop plugin.
package main

import (
	"context"

	"gitlab.com/PONCtech/meander/pkg/carriers/httpserver"
	"gitlab.com/PONCtech/meander/pkg/carriers/stdout"
	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
	"gitlab.com/PONCtech/meander/pkg/plugins/noop"
	"go.uber.org/zap"
)

const retryLimit = 10

// handleErr just panics if an error is found.
func handleErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	ctx := context.Background()
	z, err := zap.NewDevelopment()
	handleErr(err)
	logger := journal.NewZap(z)

	httpdConfig := &httpserver.Config{}
	httpdCarrier, err := httpserver.New(ctx, logger, httpdConfig, nil)
	handleErr(err)

	stdoutConfig := &stdout.Config{
		Deserialiser: stdout.WrapperPrinter,
	}
	stdoutCarrier, err := stdout.New(ctx, logger, stdoutConfig)
	handleErr(err)

	flowConfig := &flow.Config{
		RetryLimit: retryLimit,
	}

	plugins := make([]flow.Plugin, 0)
	plugins = append(plugins, noop.Plugin)

	pp := noop.PostProcessor(0)
	err = flow.Connect(logger, flowConfig, httpdCarrier, stdoutCarrier, stdoutCarrier, &pp, plugins)
	handleErr(err)
}
