/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

// This is a small demo of the most simple meander flow. It initialises a ticker
// input carrier, a printer output/error carrier, and uses the noop plugin.
package main

import (
	"context"
	"os"

	"gitlab.com/PONCtech/meander/pkg/carriers/fileio"
	"gitlab.com/PONCtech/meander/pkg/carriers/stdout"
	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
	"gitlab.com/PONCtech/meander/pkg/plugins/noop"
	"go.uber.org/zap"
)

const (
	retryLimit = 1
)

// handleErr just panics if an error is found.
func handleErr(logger journal.Logger, err error) {
	if err != nil {
		logger.Error("Failed processing file ", journal.Error(err))
		panic(err)
	}
}

func main() {
	ctx := context.Background()
	z, err := zap.NewDevelopment()
	logger := journal.NewZap(z)
	handleErr(logger, err)

	if len(os.Args) != 3 {
		logger.Error("Run with two arguments: go run examples/file2file/file2file.go <input file> <output directory>")
		os.Exit(-1)
	}

	fileReaderConfig := &fileio.Config{
		Filename: os.Args[1],
	}

	fileReaderCarrier, err := fileio.NewReader(ctx, logger, fileReaderConfig)
	handleErr(logger, err)

	fileWriterConfig := &fileio.Config{
		DirectoryName: os.Args[2],
	}

	fileWriterCarrier, err := fileio.NewWriter(ctx, logger, fileWriterConfig)
	handleErr(logger, err)

	stdoutConfig := &stdout.Config{
		Deserialiser: stdout.WrapperPrinter,
	}
	stdoutCarrier, err := stdout.New(ctx, logger, stdoutConfig)
	handleErr(logger, err)

	flowConfig := &flow.Config{
		RetryLimit: retryLimit,
	}

	plugins := make([]flow.Plugin, 0)
	plugins = append(plugins, noop.Plugin)

	pp := noop.PostProcessor(0)
	err = flow.Connect(logger, flowConfig, fileReaderCarrier, fileWriterCarrier, stdoutCarrier, &pp, plugins)
	handleErr(logger, err)
}
