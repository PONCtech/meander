/*
Copyright (c) 2022 PONC.tech

Licensed under the MIT license: https://opensource.org/licenses/MIT
Permission is granted to use, copy, modify, and redistribute the work.
Full license information available in the project LICENSE file.
*/

// This is a small demo of using meander to read from kafka. It initialises a kafka
// input carrier, a printer output/error carrier, and uses the noop plugin.
package main

import (
	"context"

	"gitlab.com/PONCtech/meander/pkg/carriers/kafka"
	"gitlab.com/PONCtech/meander/pkg/carriers/stdout"
	"gitlab.com/PONCtech/meander/pkg/flow"
	"gitlab.com/PONCtech/meander/pkg/journal"
	"gitlab.com/PONCtech/meander/pkg/plugins/noop"
	"go.uber.org/zap"
)

const retryLimit = 10

// handleErr just panics if an error is found.
func handleErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	ctx := context.Background()
	z, err := zap.NewDevelopment()
	handleErr(err)
	logger := journal.NewZap(z)

	kafkaConfig := &kafka.Config{
		Brokers:       []string{"localhost:9092"},
		InputTopic:    "tickerdata",
		ConsumerGroup: "stdoutconsumer",
		FailTopic:     "failtopic",
		// Configuration for TLS connection
		// Brokers:                    []string{"localhost:9093"},
		// CaCertificateFilename:      "./certdata/ca.pem",
		// ServiceKeyFilename:         "./certdata/service.key",
		// ServiceCertificateFilename: "./certdata/service.crt",
		// InsecureSkipVerify:         true,
	}
	kafkaCarrier, err := kafka.New(ctx, logger, kafkaConfig)
	handleErr(err)

	stdoutConfig := &stdout.Config{
		Deserialiser: stdout.WrapperPrinter,
	}
	stdoutCarrier, err := stdout.New(ctx, logger, stdoutConfig)
	handleErr(err)

	flowConfig := &flow.Config{
		RetryLimit: retryLimit,
	}

	plugins := make([]flow.Plugin, 0)
	plugins = append(plugins, noop.Plugin)

	pp := noop.PostProcessor(0)
	err = flow.Connect(logger, flowConfig, kafkaCarrier, stdoutCarrier, stdoutCarrier, &pp, plugins)
	handleErr(err)
}
